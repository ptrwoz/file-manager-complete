/** @file fileManager.c
*  @brief obsluga listy dwukierunkowej opartej na strukturze fileList
*/
#include "fileManager.h"
struct fileList* addElement(struct fileList* listA, struct fileList* elementA)
{
	if (listA == NULL)
		listA = elementA;
	else
	{
		struct fileList* a = listA;
		while (a->nextFile != NULL)
			a = a->nextFile;
		a->nextFile = elementA;
		elementA->prevFile = a;
	}
	return listA;
}
int fileNameSearch(struct fileList* elementA, char *text)
{
	int i = 0, j = 0;
	if (elementA == NULL)
		return -1;
	else
	{
		for (i = 0; i < strlen(elementA->fileName); i++)
		{
			for (j = 0; j < strlen(text); j++)
			{
				if (elementA->fileName[i + j] != text[j])
					break;
			}
			if (j == strlen(text))
				return 1;
		}
		return 0;
	}
}
struct fileList* deleteElements(struct fileList* listA, char *text)
{
	int i = 0;
	struct fileList* a = listA;
	struct fileList* b = listA;
	while (a!=NULL)
	{
		if (fileNameSearch(a, text)==0)
		{
			b = a;
			if (a->prevFile != NULL)
				a->prevFile->nextFile = a->nextFile;
			if (a->nextFile == NULL)
				break;
			a = a->nextFile;
			a->prevFile = b->prevFile;
			free(b);
		}
		else
		{		
			i = i + 1;
			if (a->nextFile != NULL)
				a = a->nextFile;
			else
				break;
		}
		
	}
	if (a == NULL)
		return NULL;
	else
	{
		while (a->prevFile != NULL)
		{
			a = a->prevFile;
		}
		if (i == 0)
			return NULL;
		else
			return a;
	}
}
struct fileList* findByPosition(struct fileList* listA, int id)
{
	int i = 0;
	struct fileList* a = listA;
	while (a != NULL)
	{
		if (i == id-1)
			return a;
		else
		{
			i++;
			a = a->nextFile;
		}
	}
	return NULL;
}
int getLengthList(struct fileList* listA)
{
	int i = 0;
	struct fileList* a = listA;
	while (a != NULL)
	{
		i = i + 1;
		a = a->nextFile;
	}
	return i;
}
int freeList(struct fileList* listA)
{
	while (listA != NULL)
	{
		struct fileList* a = listA->nextFile;
		free(listA->fileName);
		listA->fileName = NULL;
		free(listA->filePath);	
		listA->filePath = NULL;
		free(listA->fileType);
		listA->fileType = NULL;
		free(listA);
		listA = a;
	}
	return 0;
}