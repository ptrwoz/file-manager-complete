/** @file interface.h
*  @brief definicja elementow interfejsu interfacePanel	
*/
#pragma once
/**
\brief	Struktura danych interfejsu menadzera 

\details	struktura zawiera w sobie wskaznik tekstu wpisanej komendy oraz jego rozmiar
**/
struct interfacePanel	
{
	///wskaznik tekstu komendy
	char *commandChars;	
	///rozmiar tekstu komendy
	int charNumber;	
};

