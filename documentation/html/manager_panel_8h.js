var manager_panel_8h =
[
    [ "managerPanel", "structmanager_panel.html", "structmanager_panel" ],
    [ "ERROR_MANAGER", "manager_panel_8h.html#a6d2370b01eba09883852ca7949106a64", null ],
    [ "OK_MANAGER", "manager_panel_8h.html#a22fbb7781b122d21059bae45404f7f72", null ],
    [ "PATH1", "manager_panel_8h.html#a15de43864ff12c381288a52f46c8f811", null ],
    [ "PATH2", "manager_panel_8h.html#a89c297fd7a989b32feede1aaaf9008bc", null ],
    [ "addToBuffer", "manager_panel_8h.html#add5c2cec88b43c18c83b0b72d92b57e4", null ],
    [ "backPath", "manager_panel_8h.html#a51467d9860c6b26cdebd2c713f884b63", null ],
    [ "changePartition", "manager_panel_8h.html#aaea0d6ba31b93f77aa1a57033e06cb61", null ],
    [ "clearBuffer", "manager_panel_8h.html#a0db4f88ab018095150ad3c05a6b658b5", null ],
    [ "deinitPanelsAndInterface", "manager_panel_8h.html#ade9687d832c01fdb9ea604212357bfc9", null ],
    [ "drawPanels", "manager_panel_8h.html#a49269fcdad82a01a6b142490e6f10db5", null ],
    [ "exitManager", "manager_panel_8h.html#ae46c2c375b8479a2542f16feb8b63de6", null ],
    [ "getCommand", "manager_panel_8h.html#a9be6939dac977be351e8e91b6a488e1d", null ],
    [ "goPath", "manager_panel_8h.html#a0f149884010703f7d39b0eb4675529de", null ],
    [ "HandlerRoutine", "manager_panel_8h.html#a222e0ef6abfe7ef337d2d0ea71d5300d", null ],
    [ "initList", "manager_panel_8h.html#ada2ee1b8edc7b888f2156deb6b44cb0e", null ],
    [ "initManager", "manager_panel_8h.html#ad55197d688eecde284d5f1e29bd0d49d", null ],
    [ "refresh", "manager_panel_8h.html#a593880d34980c01b6129d6c5634c2de5", null ],
    [ "removeFile", "manager_panel_8h.html#aa7f7792bc8aa5049f5a2a9ffeb1a469e", null ],
    [ "renameFile", "manager_panel_8h.html#ac9f4319f1892ea510e78a799f4622645", null ],
    [ "runProgram", "manager_panel_8h.html#a39badd6760c0934b96f1520c053a2b0f", null ]
];