/** @file managerPanel.h
*  @brief definicja elementow paneli managerPanel	
*/
#pragma once
#include"dirent.h"
#define ERROR_MANAGER -1 //error com.
#define OK_MANAGER 1	//ok com.
#define PATH1 "c:\\"	//first path first panel
#define PATH2 "e:\\"	//first path secend panel

/**
\brief	Struktura panelu menadzera

\details	struktura zawiera w sobie dane pojedynczego panela w menadzerze
posiada w sobie wskaznik tekstu aktualnej sciezki, indeks wybranego elementu, status, ilosc elementow itd.
**/
struct managerPanel	
{
	///wskaznik aktualnej sciezki w panelu
	char *panelPath;	
	///indeks wybranego elementu 
	int selectedElement; 
	///status czy wybrany zostal panel 
	int statusPanel;
	///ilosc elementow w panelu
	int maxElement;	
	///struktura biblioteki dirent.h 
	DIR *dp;	
	///struktura biblioteki dirent.h 
	struct dirent *ep; 
};

///	\brief  start programu
/// \param void
/// \return 0
int runProgram(void);

///	\brief inicjalizacja panelow, interfejsu 
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int initManager(void);	

///	\brief inicjalizacja list 
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int initList(void);		

///	\brief rysowanie paneli oraz interfejsu w konsoli
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int drawPanels(void);	

///	\brief pobieranie przyciskow uzytkownika
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int getCommand(void);	

///	\brief deinicjalizacja panelow i interfejsu
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int deinitPanelsAndInterface(void);	

///	\brief przejscie do wybranej kolejnej sciezki
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int goPath(void);	

///	\brief przejscie do wybranej poprzedniej sciezki
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int backPath(void);	

///	\brief odswiezenie menadzera 
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int refresh(void); 

///	\brief zmiana nazwy elementu
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int renameFile(void); 

///	\brief usuniecie elementu
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int removeFile(void); 

///	\brief zmiana partycji w panelu
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int changePartition(void);	

///	\brief wyjscie z programu
/// \param void
/// \return ERROR_MANAGER blad
/// \return OK_MANAGER sukces
int exitManager(void);

///	\brief dodanie tekstu do bufora
/// \param text	wskaznik dodawanego tekstu
/// \return void
void addToBuffer(char* text);

///	\briefczyszczenie bufora
/// \param void
/// \return void
void clearBuffer(void);

///	\brief wykrycie wyjscia niekontrolowanego wyjscia z programu
/// \param dwCtrlType
/// \return BOOL WINAPI
BOOL WINAPI HandlerRoutine(DWORD dwCtrlType);