/** @file managerPanel.c
*  @brief obsluga operacji panelow manadzera managerPanel	
*/
#define _CRT_SECURE_NO_WARNINGS 1
#include "managerPanel.h"
#include "fileManager.h"
#include "interface.h"

///bufor konsoli 
char buffer[12801];
///wskaznik struktury listy elementow panelu 1
struct fileList *panelList1;	
///wskaznik struktury listy elementow panelu 2
struct fileList *panelList2;	
///panel 1 menadzera
struct managerPanel managerPanel1;	
///panel 2 menadzera
struct managerPanel managerPanel2;	
///interfejs menadzera
struct interfacePanel interfaceManager;	

BOOL WINAPI HandlerRoutine(DWORD dwCtrlType)
{
	deinitPanelsAndInterface();
}

int runProgram(void)
{
	SetConsoleCtrlHandler(HandlerRoutine, 1);
	if (initManager() == ERROR_MANAGER)	
	{
		printf("Init Error");
		exit(1);
	}
	while (1)
	{
		if (drawPanels() == ERROR_MANAGER)
		{
			printf("Draw Error");
			deinitPanelsAndInterface();
			exit(1);
		}
		else if (getCommand() == ERROR_MANAGER)
		{
			printf("Get Command Error");
			deinitPanelsAndInterface();
			exit(1);
		}
	}
	deinitPanelsAndInterface();
	return 0;
}

char* getPath(char* location, char* name)
{
	char* path;
	path = (char*)malloc((strlen(location))*sizeof(char) + (strlen(name))*sizeof(char) + sizeof("\\") + sizeof('\0'));
	strcpy(path, location);
	strcat(path, name);
	strcat(path, "\\");
	strcat(path, "\0");
	return path;
}

char* getFile(char* location, char* name)
{
	char* path;
	path = (char*)malloc((strlen(location))*sizeof(char) + (strlen(name))*sizeof(char) + sizeof('\0'));
	strcpy(path, location);
	strcat(path, name);
	strcat(path, "\0");
	return path;
}

int searchFile(void)
{
	char buffer[256];
	int panelNumber = 0;

	printf("Write new name file :");
	fseek(stdin, 0, SEEK_END);
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	scanf("%[^\n]s", &buffer);

	if (managerPanel1.statusPanel == 1)
	{
		panelList1 = deleteElements(panelList1, buffer);
		managerPanel1.maxElement = getLengthList(panelList1);
		if (managerPanel1.maxElement == 0)
			managerPanel1.selectedElement = -1;
		else
			managerPanel1.selectedElement = 1;
	}
	else if (managerPanel2.statusPanel == 1)
	{
		panelList2 = deleteElements(panelList2, buffer);
		managerPanel2.maxElement = getLengthList(panelList2);
		if (managerPanel2.maxElement == 0)
			managerPanel2.selectedElement = -1;
		else
			managerPanel2.selectedElement = 1;
	}
	return OK_MANAGER;
}

int renameFile(void)
{
	char buffer[256];
	char *path1 = NULL;
	char *path2 = NULL;
	char a = '\0';
	int panelNumber = 0;
	struct fileList *fl;
	fseek(stdin, 0, SEEK_END);
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	printf("Write new name file :");
	scanf("%[^\n]s", &buffer);
	do
	{
		printf("Rename file/folder? (y-yes, n-no) :");
		fseek(stdin, 0, SEEK_END);
		FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
		a = getchar();
		if (a == 'y')
		{
			if (managerPanel1.statusPanel == 1)
			{
				fl = findByPosition(panelList1, managerPanel1.selectedElement);
				panelNumber = 1;
				if (fl == NULL)
					return ERROR_MANAGER;
			}
			else if (managerPanel2.statusPanel == 1)
			{
				fl = findByPosition(panelList2, managerPanel2.selectedElement);
				panelNumber = 2;
				if (fl == NULL)
					return ERROR_MANAGER;
			}
			else
			{
				return ERROR_MANAGER;
			}
			path1 = getPath(fl->filePath, fl->fileName);
			path2 = getPath(fl->filePath, buffer);
			rename(path1, path2);
			free(path1);
			free(path2);
		}
	} while (a != 'y' && a != 'n');
	return OK_MANAGER;
}

int exitManager(void)
{
	char a = '\0';
	do
	{
		printf("Exit from manager? (y-yes) :");
		fflush(stdin);
		fseek(stdin, 0, SEEK_END);
		FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
		a = getchar();
		if (a == 'y')
		{
			deinitPanelsAndInterface();
			exit(1);
		}
		else
			return OK_MANAGER;
	} while (a != 'y');
}

int changePanelPath(char* newPath, int i)
{
	if (i == 0)
	{
		managerPanel1.selectedElement = 1;
		free(managerPanel1.panelPath);
		managerPanel1.panelPath = newPath;
	}
	else if (i == 1)
	{
		managerPanel1.selectedElement = 1;
		free(managerPanel2.panelPath);
		managerPanel2.panelPath = newPath;
	}
	else
		return ERROR_MANAGER;

}

char* managerGetchar(char* a)
{
	fflush(stdin);
	fseek(stdin, 0, SEEK_END);
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	a[0] = getchar();
	a[1] = '\0';
	return a;
}

int changePartition(void)
{
	char* path;
	char a[2];
	printf("Write partition :");
	managerGetchar(&a);
	path = (char*)malloc(sizeof(":\\\0") + sizeof(a));
	strcpy(path, a);
	strcat(path, ":\\\0");
	if (opendir(path))
	{
		int panelNumber = 0;
		if (managerPanel1.statusPanel == 1)
			changePanelPath(path, 0);
		else if (managerPanel2.statusPanel == 1)
			changePanelPath(path, 1);
	}
	else
		return ERROR_MANAGER;
}

int refresh(void)
{
	panelList1 = freeList(panelList1);
	panelList2 = freeList(panelList2);
	if (initList() == ERROR_MANAGER)
		return ERROR_MANAGER;
	managerPanel1.maxElement = getLengthList(panelList1);
	managerPanel2.maxElement = getLengthList(panelList2);
	if (managerPanel1.maxElement == 0)
		managerPanel1.selectedElement = -1;
	else
		managerPanel1.selectedElement = 1;
	if (managerPanel2.maxElement == 0)
		managerPanel2.selectedElement = -1;
	else
		managerPanel2.selectedElement = 1;
	if (drawPanels() == ERROR_MANAGER)
		return ERROR_MANAGER;
	return OK_MANAGER;
}

int elementInformation(void)
{
	struct fileList *fl;
	if (managerPanel1.statusPanel == 1)
	{
		fl = findByPosition(panelList1, managerPanel1.selectedElement);
		if (fl == NULL)
			return ERROR_MANAGER;
		printf("\n Nazwa: %s", fl->fileName);
		printf("\n Sciezka: %s%s", fl->filePath, fl->fileName);
		printf("\n Typ: %s", fl->fileType);
		switch ((fl->fileStat.st_mode & 00200))
		{
		case 0:
			printf("\n Atrybut: Tylko do odczytu");
			break;
		case 128:
			printf("\n Atrybut: Nie tylko do odczytu");
			break;
		default:
			break;
		}
		printf("\n Rozmiar(Bytes) : %ld ", fl->fileStat.st_size);
		printf("\n Rozmiar(Kilobyte): %ld ", fl->fileStat.st_size / 1024);
		printf("\n Ostatnio uzywane: %s", ctime(&fl->fileStat.st_atime));
		printf(" Ostatnia zmiana: %s", ctime(&fl->fileStat.st_ctime));
	}
	else if (managerPanel2.statusPanel == 1)
	{
		fl = findByPosition(panelList2, managerPanel2.selectedElement);
		if (fl == NULL)
			return ERROR_MANAGER;
		printf("\n Sciezka: %s%s", fl->filePath, fl->fileName);
		printf("\n Typ: %s", fl->fileType);
	}
	else
		return ERROR_MANAGER;
	fseek(stdin, 0, SEEK_END);
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
	getchar();
	return OK_MANAGER;
}

int removeFile(void)
{
	char* path;
	struct fileList *fl;
	int panelNumber = 0;
	char a = '\0';
	do
	{
		printf("Delete file/folder? (y-yes, n-no) :");
		fseek(stdin, 0, SEEK_END);
		FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
		a = getchar();
		if (a == 'y')
		{
			if (managerPanel1.statusPanel == 1)
			{
				fl = findByPosition(panelList1, managerPanel1.selectedElement);
				panelNumber = 1;
				if (fl == NULL)
					return ERROR_MANAGER;
			}
			else if (managerPanel2.statusPanel == 1)
			{
				fl = findByPosition(panelList2, managerPanel2.selectedElement);
				panelNumber = 2;
				if (fl == NULL)
					return ERROR_MANAGER;
			}
			else
			{
				return ERROR_MANAGER;
			}
			path = getPath(fl->filePath, fl->fileName);
			if (_unlink(path) == 0)
			{
				printf("Delete file complete");
			}
			else if (rmdir(path) == 0)
			{
				printf("Delete folder complete");
			}
			else
			{
				printf("Delete file error");
				free(path);
				return ERROR_MANAGER;
			}
			free(path);
			return OK_MANAGER;
		}
		else
			return OK_MANAGER;
	} while (a != 'y' && a != 'n');

}

int goPath(void)
{
	struct fileList *a;
	int panelNumber = 0;
	if (managerPanel1.statusPanel == 1)
	{
		a = findByPosition(panelList1, managerPanel1.selectedElement);
		panelNumber = 1;
		if (a == NULL)
			return ERROR_MANAGER;
	}
	else if (managerPanel2.statusPanel == 1)
	{
		a = findByPosition(panelList2, managerPanel2.selectedElement);
		panelNumber = 2;
		if (a == NULL)
			return ERROR_MANAGER;
	}
	else
	{
		return ERROR_MANAGER;
	}
	char* pathLibrary;

	char* path = getPath(a->filePath, a->fileName);
	if (pathLibrary = opendir(path))
	{
		panelList1 = freeList(panelList1);
		panelList2 = freeList(panelList2);
		if (panelNumber == 1)
		{
			managerPanel1.selectedElement = 1;
			managerPanel1.panelPath = (char*)realloc(managerPanel1.panelPath, ((strlen(path)) + 1)*sizeof(char));
			strcpy(managerPanel1.panelPath, path);
		}
		else if (panelNumber == 2)
		{
			managerPanel2.selectedElement = 1;
			managerPanel2.panelPath = (char*)realloc(managerPanel2.panelPath, ((strlen(path)) + 1)*sizeof(char));
			strcpy(managerPanel2.panelPath, path);
		}
		else
		{
			return ERROR_MANAGER;
		}
		initList();
		return OK_MANAGER;
	}
	else
	{
		return ERROR_MANAGER;
	}
	free(path);
}

int backPath()
{
	char* path;
	panelList1 = freeList(panelList1);
	panelList2 = freeList(panelList2);
	int len = 0, i = 0, j = 0;
	if (managerPanel1.statusPanel == 1)
	{

		len = strlen(managerPanel1.panelPath);
		for (i = len - 2; i > 0; i--)
		{
			if (managerPanel1.panelPath[i] == '\\' || managerPanel1.panelPath[i] == ':')
				break;
			else
				j++;
		}
		if (j != 0)
		{
			managerPanel1.panelPath = (char*)(char*)realloc(managerPanel1.panelPath, sizeof(char)*(len - j));
			managerPanel1.panelPath[len - j - 1] = '\0';
			managerPanel1.selectedElement = 1;
		}

	}
	else if (managerPanel2.statusPanel == 1)
	{

		len = strlen(managerPanel2.panelPath);
		for (i = len - 2; i > 0; i--)
		{
			if (managerPanel2.panelPath[i] == '\\' || managerPanel2.panelPath[i] == ':')
				break;
			else
				j++;
		}
		if (j != 0)
		{
			managerPanel2.panelPath = (char*)(char*)realloc(managerPanel2.panelPath, sizeof(char)*(len - j));
			managerPanel2.panelPath[len - j - 1] = '\0';
			managerPanel2.selectedElement = 1;
		}
	}
	else
	{
		return ERROR_MANAGER;
	}

	initList();
	return OK_MANAGER;
}

void addToBuffer(char* text)
{
	strcat(buffer, text);
}

void clearBuffer(void)
{
	int i = 0;
	for (i = 0; i < 12800; i++)
		buffer[i] = ' ';
	strcpy(buffer, "\0");
}

int initList(void)
{
	DIR *pDir;
	//init file list 1
	int i = 0, j = 0, k = 0;
	char typeBuffor[255];
	char* path;
	char* pathFolder;
	if ((path = opendir(managerPanel1.panelPath))) {
		managerPanel1.maxElement = 0;
		while ((managerPanel1.ep = readdir(path)))
		{
			if (managerPanel1.ep->d_name[0] != '.' && managerPanel1.ep->d_name[0] != '$')
			{
				managerPanel1.maxElement++;
				struct fileList* newItem = (struct fileList*)malloc(sizeof(struct fileList));
				newItem->filePath = (char*)malloc((strlen(managerPanel1.panelPath) + 1)*sizeof(char));
				strcpy(newItem->filePath, managerPanel1.panelPath);
				newItem->fileName = (char*)malloc((strlen(managerPanel1.ep->d_name) + 1)*sizeof(char));
				strcpy(newItem->fileName, managerPanel1.ep->d_name);
				//
				char* pathFolder = getPath(managerPanel1.panelPath, managerPanel1.ep->d_name);
				if (pDir = opendir(pathFolder))
				{
					newItem->fileType = (char*)malloc((strlen("folder") + 1) * sizeof(char));
					strcpy(newItem->fileType, "folder");
					closedir(pDir);
				}
				else
				{
					k = 0;
					j = 0;
					for (i = 0; i < strlen(managerPanel1.ep->d_name); i++)
					{
						if (managerPanel1.ep->d_name[i] == '.')
						{
							k = 1;
							j = 0;
						}
						typeBuffor[j] = managerPanel1.ep->d_name[i];
						j++;
					}
					if (k == 1)
					{
						typeBuffor[j] = '\0';
						newItem->fileType = (char*)malloc((strlen(typeBuffor) + 1)  * sizeof(char));
						strcpy(newItem->fileType, typeBuffor);
					}
					else
					{
						newItem->fileType = (char*)malloc((strlen("NULL") + 1)  * sizeof(char));
						strcpy(newItem->fileType, "NULL");
					}
				}

				char *pathFile = getFile(managerPanel1.panelPath, managerPanel1.ep->d_name);
				if ((stat(pathFile, &newItem->fileStat)) == -1)
					continue;
				free(pathFile);
				free(pathFolder);
				newItem->nextFile = NULL;
				newItem->prevFile = NULL;
				panelList1 = addElement(panelList1, newItem);
			}
		}
		if (managerPanel1.maxElement == 0)
			managerPanel1.selectedElement = -1;
		closedir(path);
	}
	else
	{
		//init error
		return ERROR_MANAGER;
	}
	//init file list2
	if ((path = opendir(managerPanel2.panelPath))) {
		managerPanel2.maxElement = 0;
		while ((managerPanel2.ep = readdir(path)))
		{
			if (managerPanel2.ep->d_name[0] != '.' && managerPanel2.ep->d_name[0] != '$')
			{
				managerPanel2.maxElement++;
				struct fileList* newItem = (struct fileList*)malloc(sizeof(struct fileList));
				newItem->filePath = (char*)malloc((strlen(managerPanel2.panelPath) + 1)*sizeof(char));
				strcpy(newItem->filePath, managerPanel2.panelPath + '\0');
				newItem->fileName = (char*)malloc((strlen(managerPanel2.ep->d_name) + 1)*sizeof(char));
				strcpy(newItem->fileName, managerPanel2.ep->d_name + '\0');
				char* pathFolder = getPath(managerPanel2.panelPath, managerPanel2.ep->d_name);
				if (pDir = opendir(pathFolder))
				{
					newItem->fileType = (char*)malloc((strlen("folder") + 1)  * sizeof(char));
					strcpy(newItem->fileType, "folder");
					closedir(pDir);
				}
				else
				{
					k = 0;
					j = 0;
					for (i = 0; i < strlen(managerPanel2.ep->d_name); i++)
					{
						if (managerPanel2.ep->d_name[i] == '.')
						{
							k = 1;
							j = 0;
						}
						typeBuffor[j] = managerPanel2.ep->d_name[i];
						j++;
					}
					if (k == 1)
					{
						typeBuffor[j] = '\0';
						newItem->fileType = (char*)malloc((strlen(typeBuffor) + 1)  * sizeof(char));
						strcpy(newItem->fileType, typeBuffor);
					}
					else
					{
						newItem->fileType = (char*)malloc((strlen("null") + 1)  * sizeof(char));
						strcpy(newItem->fileType, "null");
					}
				}
				free(pathFolder);
				newItem->nextFile = NULL;
				newItem->prevFile = NULL;
				panelList2 = addElement(panelList2, newItem);
			}
		}
		if (managerPanel2.maxElement == 0)
			managerPanel2.selectedElement = -1;
		closedir(path);
	}
	else
	{
		return ERROR_MANAGER;
	}
	return OK_MANAGER;
}

int initManager(void)
{
	//init interface
	interfaceManager.charNumber = 1;
	interfaceManager.commandChars = (char*)malloc(sizeof(">"));
	strcpy(interfaceManager.commandChars, ">");
	//init panel 1
	panelList1 = NULL;
	managerPanel1.maxElement = 0;
	managerPanel1.statusPanel = 1;
	managerPanel1.selectedElement = 1;
	managerPanel1.panelPath = (char*)malloc(sizeof(PATH1));
	strcpy(managerPanel1.panelPath, PATH1);
	//init panel 2
	panelList2 = NULL;
	managerPanel2.maxElement = 0;
	managerPanel2.statusPanel = 0;
	managerPanel2.selectedElement = 1;
	managerPanel2.panelPath = (char*)malloc(sizeof(PATH2));
	strcpy(managerPanel2.panelPath, PATH2);
	//
	initList();
	return OK_MANAGER;
};

int drawPanels(void)
{
	struct fileList* a = panelList1;
	struct fileList* b = panelList2;
	int i = 1, j = 1;
	clearBuffer();	//clear buffor
	addToBuffer(managerPanel1.panelPath);	//draw path 1 panel
	for (j = 0 + strlen(managerPanel1.panelPath); j < 58; j++)
		addToBuffer(" ");
	addToBuffer(" || ");
	addToBuffer(managerPanel2.panelPath);
	addToBuffer("\n");	//draw line
	for (j = 0; j < 100; j++)
		addToBuffer("_");
	addToBuffer("\n");
	while (a != NULL || b != NULL)	// draw panel1 and panel2
	{
		if (managerPanel1.statusPanel == 1 && i == managerPanel1.selectedElement)	//draw panel1 cursor
			addToBuffer("->");
		if (a != NULL)	//draw full position in panel1 
		{
			addToBuffer(a->fileName);	//draw fiile or folder name
			if (managerPanel1.statusPanel == 1 && i == managerPanel1.selectedElement)
			{
				for (j = 0 + strlen(a->fileName); j < 60 - 2; j++)
					addToBuffer(" ");
				addToBuffer("|");
			}
			else
			{
				for (j = 0 + strlen(a->fileName); j < 60; j++)
					addToBuffer(" ");
				addToBuffer("|");
			}
			a = a->nextFile;	//next element in list
		}
		else  //draw empty position in panel 
		{
			for (j = 0; j < 60; j++)
				addToBuffer(" ");
			addToBuffer("|");
		}
		if (managerPanel2.statusPanel == 1 && i == managerPanel2.selectedElement)	//draw panel2 cursor
			addToBuffer("->");
		if (b != NULL)	//draw full position in panel2 
		{
			addToBuffer(b->fileName);	//draw file or folder name
			b = b->nextFile;	//next element in list
		}
		addToBuffer("\n");
		i++;
	}
	for (j = 0; j < 80; j++)	//draw line		
		addToBuffer("_");
	addToBuffer("\n");
	addToBuffer("Q-refresh, C-change partition, R-rename, D-remove, S-search, TAB-back, F-forward");
	addToBuffer("\n");
	addToBuffer(interfaceManager.commandChars);	//draw command line 
	system("cls");
	printf("%s", buffer);
	return OK_MANAGER;
};

int getCommand(void)
{
	int i = 0;
	unsigned char KeyStates[256];
	GetKeyboardState(KeyStates);
	Sleep(100);
	while (1)
	{

		if (GetAsyncKeyState(VK_UP))	//arrow up
		{
			if (managerPanel1.statusPanel != 0)
			{
				if (managerPanel1.selectedElement > 1 && managerPanel1.selectedElement != -1)
					managerPanel1.selectedElement--;
				else if (managerPanel1.selectedElement != -1)
					managerPanel1.selectedElement = managerPanel1.maxElement;
			}
			else if (managerPanel2.statusPanel != 0)
			{
				if (managerPanel2.selectedElement>1 && managerPanel2.selectedElement != -1)
					managerPanel2.selectedElement--;
				else if (managerPanel2.selectedElement != -1)
					managerPanel2.selectedElement = managerPanel2.maxElement;
			}
			break;
		}
		else if (GetAsyncKeyState(VK_DOWN))	//arrow down
		{
			if (managerPanel1.statusPanel != 0)
			{
				if (managerPanel1.maxElement > managerPanel1.selectedElement && managerPanel1.selectedElement != -1)
					managerPanel1.selectedElement++;
				else if (managerPanel1.selectedElement != -1)
					managerPanel1.selectedElement = 1;
			}
			else if (managerPanel2.statusPanel != 0)
			{
				if (managerPanel2.maxElement>managerPanel2.selectedElement && managerPanel2.selectedElement != -1)
					managerPanel2.selectedElement++;
				else if (managerPanel2.selectedElement != -1)
					managerPanel2.selectedElement = 1;
			}
			break;
		}
		else if (GetAsyncKeyState(VK_LEFT))
		{
			if (managerPanel2.statusPanel == 1)
			{
				managerPanel1.statusPanel = 1;
				managerPanel2.statusPanel = 0;
			}
			break;
		}
		else if (GetAsyncKeyState(VK_RIGHT))
		{
			if (managerPanel1.statusPanel == 1)
			{
				managerPanel1.statusPanel = 0;
				managerPanel2.statusPanel = 1;
			}
			break;
		}
		else if (GetAsyncKeyState(VK_TAB))
		{
			if (backPath() == ERROR_MANAGER)
			{
				printf("Back to path error");
				refresh();
			}
			break;
		}
		else if (GetAsyncKeyState(VK_ESCAPE))
		{
			exitManager();
			break;
		}
		else
		{
			for (i = 0; i < 256; i++)
			{

				if (GetAsyncKeyState(i) == -32767 && i != 1)
				{
					if (((char)i) == 'R')	//rename file
					{
						renameFile();
						refresh();
						break;
					}
					else if (((char)i) == 'Q')	//refresh manager
					{
						refresh();
						break;
					}
					else if (((char)i) == 'I')	//information element
					{
						elementInformation();
						refresh();
						break;
					}
					else if (((char)i) == 'C')	//change partition
					{
						changePartition();
						refresh();
						break;
					}
					else if (((char)i) == 'F')	//delete file
					{
						if (goPath() == ERROR_MANAGER)
						{
							printf("Go to path error");
						}
						refresh();
						break;
					}
					else if (((char)i) == 'S')	//search element
					{
						searchFile();
						drawPanels();
						//refresh();
						break;
					}
					else if (((char)i) == 'D')	//delete file
					{
						removeFile();
						refresh();
						break;
					}
				}
			}
		}
	}
	return OK_MANAGER;
};

int deinitPanelsAndInterface(void)
{
	freeList(panelList1);
	freeList(panelList2);
	return OK_MANAGER;
}
