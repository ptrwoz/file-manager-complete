var file_manager_8h =
[
    [ "fileList", "structfile_list.html", "structfile_list" ],
    [ "addElement", "file_manager_8h.html#a6657cd6a8a8fc11009c04aaf576fff3b", null ],
    [ "deleteElements", "file_manager_8h.html#a02133e5bfdf94580827b8da6916604fa", null ],
    [ "fileNameSearch", "file_manager_8h.html#a36dd4f668663b3847f49de91cd6aa113", null ],
    [ "findByPosition", "file_manager_8h.html#a832cbff741b9dcfe5da23d465463dea9", null ],
    [ "freeList", "file_manager_8h.html#a206647bab2108ea2aba536edcdcd5c00", null ],
    [ "getLengthList", "file_manager_8h.html#ab52406007100ffcfc5c718ac6ed0bf5d", null ]
];