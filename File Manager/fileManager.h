/** @file fileManager.h
*  @brief definicja elementow listy dwukierunkowej fileList	
*/
#pragma once
#include <Windows.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>
/**
	\brief	Struktura elementu listy dwukierunkowej przechowujaca dane

	\details	struktura zawiera w sobie wskazniki wskazujace na element poprzedzajacy i nastepujacy w liscie
	posiada pola danych elemencie takie jak sciezka, nazwa, typ itd.
**/
struct fileList	
{
	///struktura statystyk elementu
	struct stat fileStat;
	///wskaznik nastepnego elementu 
	struct fileList* prevFile;	
	///wskaznik poprzedniego elementu 
	struct fileList* nextFile;	
	///sciezka elementu
	char* filePath;	
	///nazwa elementu
	char* fileName; 
	///type elementu 
	char* fileType;
};
///	\brief dodawanie elementu do listy dwukierunkowej
/// \param listA wskaznik listy	
/// \param elementA wskaznik dodawanego elementu
/// \return wskaznik listy po dodaniu elementu
struct fileList* addElement(struct fileList* listA, struct fileList* elementA);

///	\brief znalezienie elementu listy dwukierunkowej o okreslonej pozycji 
/// \param listA wskaznik listy		
/// \param id numer elementu szukanego
/// \return wskaznik aktualnej listy
struct fileList* findByPosition(struct fileList* listA, int id);

///	\brief usuniecie elementow z listy o nazwie w ktorej nie znajduje sie tekst
/// \param listA wskaznik listy		
/// \param text wskaznik tekstu
/// \return wskaznik aktualnej listy
struct fileList* deleteElements(struct fileList* listA, char *text);

///	\brief okreslenie czy tekst znajduje sie nazwie elementu
/// \param elementA wskaznik elementu listy		
/// \param text tekst do znalezienia w nazwie
/// \return -1 element to NULL,			
/// \return 0 nie znaleziono tekstu	w nazwie
/// \return 1 znaleziono tekst w nazwie
int fileNameSearch(struct fileList* elementA, char *text);

///	\brief usuniecie wszystkich elementu listy dwukierunkowej
/// \param wskaznik listy
/// \return 0
int freeList(struct fileList* listA);

///	\brief zwrocenie ilosci elementow listy
/// \param wskaznik listy
/// \return ilosci elementow listy
int getLengthList(struct fileList* listA);